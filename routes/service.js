const express = require('express');
const router = express.Router();
const auth = require('../auth');
const serviceController = require('../controllers/service');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
	if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
		cb(null, true);
	}else cb(null, false);
}

const upload = multer({
	storage: storage, 
	limits:{
		fileSize:1024 * 1024 * 5
	},
	fileFilter: fileFilter
});
// upload.single('productImage')

router.get('/', (req, res) => {
	productController.showAllServices().then(resultFromController => res.send(resultFromController))
})

// admin function - add a service to the db
router.post('/add-services', auth.verify, upload.single('serviceImage'), (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == true) {
	serviceController.addServices(req.body, req.file).then(resultFromController => res.send(resultFromController))		
	} else console.log('not authorized'); return false
	
});

router.post('/archive/:id', auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == true) {
	serviceController.archiveService(req.params, req.body).then(resultFromController => res.send(resultFromController))	
	} else console.log('not authorized'); return false
})

// admin function - update a service to the db
router.patch('/update-services/:id', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == true) {
	serviceController.updateServices(req.params.id,req.body).then(resultFromController => res.send(resultFromController))		
	} else console.log('not authorized'); return false
	
});


// book a schedule
router.post('/booking', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	let data = {
		userId: userData.id,
		serviceId: req.body.productId,
		setDate: req.body.setDate,
		time: req.body.time,
		venue: req.body.venue

	} 
	if(userData.isAdmin == false) {
	serviceController.booking(data).then(resultFromController => res.send(resultFromController))	
	} else {
		return false
	} 
});

module.exports = router;