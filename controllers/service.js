const Booking = require('../models/booking');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Service = require('../models/service');
const User = require('../models/user');

module.exports.addServices = (serviceDetails) => {
	const {name, description, price} = serviceDetails;

	let newService = new Service({
		name: name,
		description: description,
		price: price
	})
	return newService.save().then((service, err) => {
		if(err) return false
		else return service
	})

}

module.exports.updateServices = (serviceId, serviceDetails) => {
	const {name, description, price} = serviceDetails;

	let updatedService = {
		name: name,
		description: description,
		price: price
	}
	return Service.findByIdAndUpdate(serviceId, updatedService).then((service, err) => {
		if(err) return false
		else return service
	})

}

// Show Available Service
exports.showAllServices = () => {
	return Service.find({}).then(service => {
		return service
	})
};


// Archive service
exports.archiveService = (serviceId, reqBody) => {
	const { id } = serviceId;
	return Service.findById(id).then(service => {
		console.log(service)
		service.status = reqBody.status
		return service.save().then((service, err) => {
			if(err){ console.log(err); return false}
			else return service
		})
	})	
}




// book a schedule
exports.booking = async(data) => {

	let service = await Service.findById(data.productId)
	let user = await User.findById(data.userId);
	let isBookingUpdated = await Booking.find({}).then((result, err) => {
		  let newBooking = new Booking({
		  		userId: user.id,
		  		serviceId: data.serviceId,
		  		setDate: data.setDate,
		  		time: data.time,
		  		venue: data.venue,
		  		price: service.price
		 	});
				console.log(newBooking);
				  return newBooking.save().then((service, err) => {
				  	if (err) {
				  		console.log(err)
				  		return false
				  	} else {
				  		return true
				  	}
				  })
		})  
		 
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.bookings.push({servicetId: data.serviceId})
		return user.save().then((user,err) => {
			if(err) return false
			else return true
		})	
	});

	let isServiceUpdated = await Service.findById(data.serviceId).then(service => {
		service.bookings.push({userId: data.userId})
		return service.save().then((service, err) => {
			if(err) return false
			else return true
		})
	})


	if(isUserUpdated && isProductUpdate && isOrderUpdated) return true
	else return false
}